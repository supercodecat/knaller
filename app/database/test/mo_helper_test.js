const mongoose = require('mongoose');

mongoose.Promise = global.Promise; // ESG - promise

// establishing a connection

mongoose.set('useUnifiedTopology', true);
mongoose.connect('mongodb://localhost/mongorecipes', {
  useNewUrlParser: true
});

mongoose.connection
  // eslint-disable-next-line no-console
  .once('open', () => console.log('Connected'))
  .on('error', error => {
    // eslint-disable-next-line no-console
    console.log('Your error', error);
  });
