const mongoose = require('mongoose');

const { Schema } = mongoose;

const RecipeSchema = new Schema({
  name: String
});

const Recipe = mongoose.model('recipe', RecipeSchema);

// export
module.exports = Recipe;
